import requests
import json
import logging

logger=logging.getLogger("METADATA-UPDATER")
class MetaPoc:
    def __init__(self, hostname):

        self.hostname = hostname
        self.session = None

    def get_entity_metadata(self, entity_type, entity_id):
        res = ""
        try:
            url = "http://{0}/api/v2/{1}/{2}/metadata".format(self.hostname, entity_type, entity_id)
            print("url:" + url)
            session_obj = self.session
            res = session_obj.get(url)
            print(res.text)
            return res.text
        except requests.HTTPError as httperror:
            logger.error(
                "Error While metadata creation for entity type : {0} => {1}".format(entity_type, httperror))

    def update_with_previous_metadata(self, entity_id, entity_type, data):
        """
        This method will updates the existing value and not add metadata eliminate "Multiple values detected"
        :param entity_id:
        :param entity_type:
        :param datum: { "Namespace.Attribute": "value" }
        :return:
        """
        nms_metadata = self.get_entity_metadata(entity_id=entity_id, entity_type=entity_type)

        meta = json.loads(nms_metadata)

        if "{system}.Dispatch Identifier" in meta:
            del meta["{system}.Dispatch Identifier"]

        print(meta)

        for attributes in data:
            for attribute in attributes:
                print(attribute)
                meta[attribute] = meta.get(attribute, {"values": [{"value": ""}]})

                meta[attribute]["values"][0]["value"] = attributes[attribute]["values"][0]["value"]

        print(meta)
        res = self.set_entity_metadata(entity_type="devices", entity_id=entity_id, data=meta)
        print(res)

    def signin(self, username, password):
        head = {'Accept': 'application/json', 'Content-Type': 'application/json'
                }
        url = "http://{0}/api/v2/authentication/signin?nmsLogin=false".format(self.hostname)
        data = {
            "name": username,
            "password": password
        }
        data=json.dumps(data)

        session = requests.Session()
        session.headers = head
        res = session.post(url=url, data=data)
        print(res.text)
        return res.text

    def set_entity_metadata(self, entity_type, entity_id, data):
        """
        Creating device group for given device_name and parent_id.
        """
        res = ""
        try:

            url = "http://{0}/api/v2/{1}/{2}/metadata".format(self.hostname, entity_type, entity_id)
            print("url:" + url)
            data = json.dumps(data)
            print("data--->" + data)
            session_obj=self.session
            res = session_obj.put(url=url, data=data)
            print("success")
            return res

        except requests.HTTPError as httperror:
            logger.error("Error While metadata creation for entity type : {0} => {1}".format(entity_type, httperror))


if __name__ == '__main__':

    print("\n----------Update metadata for device--------\n")
    host = input("Enter the Nms Hostname: ")
    device_id = input("Enter the device id: ")
    username=input("Enter the username :")
    password=input("Enter the password : ")
    meta = MetaPoc(hostname=host)
    sign = json.loads(meta.signin(username,password))
    authKey=sign["token"]
    # data = input("Enter the Data in json format: \n")

    logger.info("signed-in successfully")
    head = {'Accept': 'application/json', 'Content-Type': 'application/json',
            'X-AUTH-TOKEN': authKey}

    session = requests.Session()
    session.headers = head
    meta.session = session
    data=[{"CUSTOMER_META_DATA.latitude": {
            "attributeType": "string",
            "values": [
                {"value": "18766.0212"}
            ]
        }}, {"CUSTOMER_META_DATA.longitude": {
            "attributeType": "string",
            "values": [
                {"value": "10.0212"}
            ]
        }}]

    meta.update_with_previous_metadata(entity_id=device_id, entity_type="devices", data=data)





